# |/usr/bin/env python3

"""Sort a list of formats according to their level of compression"""

import sys

# Ordered list of image formats, from lower to higher compression
# (assumed classification)
fordered: tuple = ("raw", "bmp", "tiff", "ppm", "eps", "tga",
                   "pdf", "svg", "png", "gif", "jpeg", "webp")


def lower_than(format1: str, format2: str) -> bool:
    """Find out if format1 is lower than format2
    Returns True if format1 is lower, False otherwise.
    A format is lower than other if it is earlier in the fordered list.
    """
    position1 = fordered.index(format1)
    position2 = fordered.index(format2)
    return position1 < position2


def find_lower_pos(formats: list, pivot: int) -> int:
    """Find the lower format in formats after pivot
    Returns the index of the lower format found"""
    lower: int = pivot
    for pos in range(pivot+1, len(formats)):
        if lower_than(formats[pos], formats[lower]):
            lower = pos

    return lower


def sort_formats(formats: list) -> list:
    """Sort formats list
    Returns the sorted list of formats, according to their
    position in fordered"""
    for pivot_pos in range(len(formats)):
        lower_pos: int = find_lower_pos(formats, pivot_pos)
        if lower_pos != pivot_pos:
            (formats[pivot_pos], formats[lower_pos]) = (formats[lower_pos], formats[pivot_pos])

    return formats


def main():
    """Read command line arguments, and print them sorted
    Also, check if they are valid formats using the fordered tuple"""

    formats: list = sys.argv[1:]
    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")
    sorted_formats: list = sort_formats(formats)
    for i, format in enumerate(sorted_formats):
        if i < len(sorted_formats) - 1:
            print(format, end=" ")
        else:
            print(format, end="\n")


if __name__ == '__main__':
    main()